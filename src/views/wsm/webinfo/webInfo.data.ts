import type { BasicColumn, FormSchema } from "@/components/Table";
import { useRender } from "@/components/Table";

export const columns: BasicColumn[] = [
  {
    title: "标签页名称",
    dataIndex: "tabsName",
    width: 160,
  },
  {
    title: "favicon ",
    dataIndex: "favicon",
    width: 160,
  },
  {
    title: "网站名称",
    dataIndex: "webName",
    width: 160,
  },
  {
    title: "网站logo",
    dataIndex: "webLogo",
    width: 160,
  },
  {
    title: "简介一",
    dataIndex: "introduceOne",
    width: 160,
  },
  {
    title: "简介二",
    dataIndex: "introduceTwo",
    width: 160,
  },
  {
    title: "QQ",
    dataIndex: "qq",
    width: 160,
  },
  {
    title: "微信",
    dataIndex: "wechat",
    width: 160,
  },
  {
    title: "邮箱",
    dataIndex: "email",
    width: 160,
  },
  {
    title: "版权所有",
    dataIndex: "copyright",
    width: 160,
  },
  {
    title: "备案号",
    dataIndex: "icp",
    width: 160,
  },
  {
    title: "公安备案",
    dataIndex: "publicEcurity",
    width: 160,
  },
  {
    title: "绑定用户",
    dataIndex: "bindingUserName",
    width: 160,
    auth: "wsm:web-info:create",
  },
  {
    title: "创建时间",
    dataIndex: "createTime",
    width: 180,
    ifShow: false,
    customRender: ({ text }) => {
      return useRender.renderDate(text);
    },
  },
  {
    title: "修改人",
    dataIndex: "updaterName",
    width: 160,
  },
  {
    title: "修改时间",
    dataIndex: "updateTime",
    width: 180,
    customRender: ({ text }) => {
      return useRender.renderDate(text);
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    label: "标签页名称",
    field: "tabsName",
    component: "Input",
    colProps: { span: 6 },
  },
  {
    label: "favicon ",
    field: "favicon",
    component: "Input",
    colProps: { span: 6 },
  },
  {
    label: "网站名称",
    field: "webName",
    component: "Input",
    colProps: { span: 6 },
  },
];

export const createFormSchema: FormSchema[] = [
  {
    label: "编号",
    field: "id",
    show: false,
    component: "Input",
  },
  {
    label: "标签页名称",
    field: "tabsName",
    required: true,
    component: "Input",
    defaultValue: "标签页名称",
  },
  {
    label: "favicon ",
    field: "favicon",
    required: true,
    component: "Input",
  },
  {
    label: "网站名称",
    field: "webName",
    required: true,
    component: "Input",
    defaultValue: "网站名称",
  },
  {
    label: "网站logo",
    field: "webLogo",
    component: "Input",
  },
  {
    label: "简介一",
    field: "introduceOne",
    component: "Input",
  },
  {
    label: "简介二",
    field: "introduceTwo",
    component: "Input",
  },
  {
    label: "QQ",
    field: "qq",
    component: "Input",
  },
  {
    label: "微信",
    field: "wechat",
    component: "Input",
  },
  {
    label: "邮箱",
    field: "email",
    component: "Input",
  },
  {
    label: "版权所有",
    field: "copyright",
    component: "Input",
  },
  {
    label: "备案号",
    field: "icp",
    component: "Input",
  },
  {
    label: "公安备案",
    field: "publicEcurity",
    component: "Input",
  },
  {
    label: "绑定用户",
    field: "bindingUserId",
    required: true,
    component: "Input",
  },
];

export const updateFormSchema: FormSchema[] = [
  {
    label: "编号",
    field: "id",
    show: false,
    component: "Input",
  },
  {
    label: "标签页名称",
    field: "tabsName",
    required: true,
    component: "Input",
  },
  {
    label: "favicon ",
    field: "favicon",
    required: true,
    component: "Input",
  },
  {
    label: "网站名称",
    field: "webName",
    required: true,
    component: "Input",
  },
  {
    label: "网站logo",
    field: "webLogo",
    component: "Input",
  },
  {
    label: "简介一",
    field: "introduceOne",
    component: "Input",
  },
  {
    label: "简介二",
    field: "introduceTwo",
    component: "Input",
  },
  {
    label: "QQ",
    field: "qq",
    component: "Input",
  },
  {
    label: "微信",
    field: "wechat",
    component: "Input",
  },
  {
    label: "邮箱",
    field: "email",
    component: "Input",
  },
  {
    label: "版权所有",
    field: "copyright",
    component: "Input",
  },
  {
    label: "备案号",
    field: "icp",
    component: "Input",
  },
  {
    label: "公安备案",
    field: "publicEcurity",
    component: "Input",
  },
  {
    label: "绑定用户",
    field: "bindingUserId",
    required: true,
    component: "Input",
  },
];
