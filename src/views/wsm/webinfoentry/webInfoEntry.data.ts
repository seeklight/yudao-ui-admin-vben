import type { BasicColumn, FormSchema } from "@/components/Table";
import { useRender } from "@/components/Table";
import { DICT_TYPE, getDictOptions } from "@/utils/dict";

export const columns: BasicColumn[] = [
  {
    title: "网站图标",
    dataIndex: "webIcon",
    width: 160,
  },
  {
    title: "网站名称",
    dataIndex: "webName",
    width: 160,
  },
  {
    title: "网站链接",
    dataIndex: "webLink",
    width: 160,
  },
  {
    title: "排序",
    dataIndex: "webSort",
    width: 50,
  },
  {
    title: "状态",
    dataIndex: "status",
    width: 100,
    customRender: ({ text }) => {
      return useRender.renderDict(text, DICT_TYPE.COMMON_STATUS);
    },
  },
  // {
  //   title: '创建时间',
  //   dataIndex: 'createTime',
  //   width: 180,
  //   customRender: ({ text }) => {
  //     return useRender.renderDate(text)
  //   },
  // },
  {
    title: "绑定用户",
    dataIndex: "bindingName",
    width: 160,
    auth: "wsm:web-info:create",
  },
  {
    title: "修改人",
    dataIndex: "updaterName",
    width: 160,
  },
  {
    title: "修改时间",
    dataIndex: "updateTime",
    width: 180,
    customRender: ({ text }) => {
      return useRender.renderDate(text);
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    label: "网站名称",
    field: "webName",
    component: "Input",
    colProps: { span: 8 },
  },
  {
    label: "网站链接",
    field: "webLink",
    component: "Input",
    colProps: { span: 8 },
  },
  {
    label: "状态",
    field: "status",
    component: "Select",
    componentProps: {
      options: getDictOptions(DICT_TYPE.COMMON_STATUS),
    },
    colProps: { span: 8 },
  },
];

export const createFormSchema: FormSchema[] = [
  {
    label: "编号",
    field: "id",
    show: false,
    component: "Input",
  },
  {
    label: "网站图标",
    field: "webIcon",
    component: "Input",
  },
  {
    label: "网站名称",
    field: "webName",
    required: true,
    component: "Input",
  },
  {
    label: "网站链接",
    field: "webLink",
    required: true,
    component: "Input",
  },
  {
    label: "排序",
    field: "webSort",
    required: true,
    component: "Input",
  },
  {
    label: "状态",
    field: "status",
    required: true,
    component: "Select",
    componentProps: {
      options: getDictOptions(DICT_TYPE.COMMON_STATUS, "number"),
    },
  },
];

export const updateFormSchema: FormSchema[] = [
  {
    label: "编号",
    field: "id",
    show: false,
    component: "Input",
  },
  {
    label: "网站图标",
    field: "webIcon",
    component: "Input",
  },
  {
    label: "网站名称",
    field: "webName",
    required: true,
    component: "Input",
  },
  {
    label: "网站链接",
    field: "webLink",
    required: true,
    component: "Input",
  },
  {
    label: "排序",
    field: "webSort",
    required: true,
    component: "Input",
  },
  {
    label: "状态",
    field: "status",
    required: true,
    component: "Select",
    componentProps: {
      options: getDictOptions(DICT_TYPE.COMMON_STATUS, "number"),
    },
  },
];
