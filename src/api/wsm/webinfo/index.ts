import { defHttp } from '@/utils/http/axios'

// 查询网站信息列表
export function getWebInfoPage(params) {
  return defHttp.get({ url: '/wsm/web-info/page', params })
}

// 查询网站信息详情
export function getWebInfo(id: number) {
  return defHttp.get({ url: `/wsm/web-info/get?id=${id}` })
}

// 新增网站信息
export function createWebInfo(data) {
  return defHttp.post({ url: '/wsm/web-info/create', data })
}

// 修改网站信息
export function updateWebInfo(data) {
  return defHttp.put({ url: '/wsm/web-info/update', data })
}

// 删除网站信息
export function deleteWebInfo(id: number) {
  return defHttp.delete({ url: `/wsm/web-info/delete?id=${id}` })
}

// 导出网站信息 Excel
export function exportWebInfo(params) {
  return defHttp.download({ url: '/wsm/web-info/export-excel', params }, '网站信息.xls')
}