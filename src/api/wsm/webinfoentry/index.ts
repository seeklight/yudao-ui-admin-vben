import { defHttp } from '@/utils/http/axios'

// 查询网站列表列表
export function getWebInfoEntryPage(params) {
  return defHttp.get({ url: '/wsm/web-info-entry/page', params })
}

// 查询网站列表详情
export function getWebInfoEntry(id: number) {
  return defHttp.get({ url: `/wsm/web-info-entry/get?id=${id}` })
}

// 新增网站列表
export function createWebInfoEntry(data) {
  return defHttp.post({ url: '/wsm/web-info-entry/create', data })
}

// 修改网站列表
export function updateWebInfoEntry(data) {
  return defHttp.put({ url: '/wsm/web-info-entry/update', data })
}

// 删除网站列表
export function deleteWebInfoEntry(id: number) {
  return defHttp.delete({ url: `/wsm/web-info-entry/delete?id=${id}` })
}

// 导出网站列表 Excel
export function exportWebInfoEntry(params) {
  return defHttp.download({ url: '/wsm/web-info-entry/export-excel', params }, '网站列表.xls')
}